# Notes App

Notes app for alternative organization methods.

Allows creating and connecting windows to each other in unique ways
* can collapse connected windows
  * maybe allow the screen to be split into a top and a bottom half, for when you want to link previously unrelated sections together
* for a lot of nested notes
  * maybe with arrows being able to be pointed to non linearly connected parts, to show ideas (something deep in the nesting with an arrow connecting to another part)
* Colorblind mode available for launch, with the option being toggleable for the end user.

## Examples

### __Game Idea__

![Game Example](game.png)

Colors used to showcase granular windows

This was the main idea, for allowing you to create nested notes, with potential functionality of allowing hard links to sections from within non-nested sections.

As an example of this, each character in this rpg could start as a specific class, so in the classes section, you could have a link from that characters starting class to the character.

### __Collaborative Workflow__

A future goal would be allowing these notes to be collaborative. 

Instead of using a 'live' implementation as seen in google documents and elsewhere, users would be able to open side windows that are attached to specific sections, which they would be able to 'push' to the main window once complete. 

This functionality helps the user in cases of many users editing a single document, where using other software options your frame of reference might constantly shift, which can be jarring while writing. If wanted, it also allows for other collaborators to view a users' section before it's added to the main document.

![Collab Example](collab.png)